﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ETG_BattlePointTracker
{
	// Humbly borrowed from: https://github.com/An3s079/DPSMod/blob/main/DPSTeller.cs
	class PointUpdater : MonoBehaviour
	{
		void Update()
		{
			BattlePointTracker.PointsText.text = $"Points: {BattlePointTracker.Points}";
		}
	}
}
