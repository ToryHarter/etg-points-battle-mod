﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using UnityEngine;

namespace ETG_BattlePointTracker
{
	// Based off the SpeedrunTimer.cs implementation
    class CountdownTimer : MonoBehaviour
    {
        private char[] _formattedTimeSpan;

		private UnityEngine.UI.Text _countdownTimerText;

        public CountdownTimer()
        {
            this._formattedTimeSpan = new char[11];
        }

        private void Start()
        {
			this._countdownTimerText = BattlePointTracker.CountdownTimer;
		}

        private void Update()
        {
			int num = Mathf.FloorToInt(BattlePointTracker.TimerRemaining);
			int num2 = num / 3600;
			int num3 = num / 60 % 60;
			int num4 = num % 60;
			int num5 = Mathf.FloorToInt(1000f * (BattlePointTracker.TimerRemaining % 1f));
			int num6 = 48;
			this._formattedTimeSpan[0] = (char)(num6 + num2 % 10);
			this._formattedTimeSpan[1] = ':';
			this._formattedTimeSpan[2] = (char)(num6 + num3 / 10 % 10);
			this._formattedTimeSpan[3] = (char)(num6 + num3 % 10);
			this._formattedTimeSpan[4] = ':';
			this._formattedTimeSpan[5] = (char)(num6 + num4 / 10 % 10);
			this._formattedTimeSpan[6] = (char)(num6 + num4 % 10);
			this._formattedTimeSpan[7] = '.';
			this._formattedTimeSpan[8] = (char)(num6 + num5 / 100 % 10);
			this._formattedTimeSpan[9] = (char)(num6 + num5 / 10 % 10);
			this._formattedTimeSpan[10] = (char)(num6 + num5 % 10);
			this._countdownTimerText.text = new string(this._formattedTimeSpan);
		}

		private void LateUpdate()
        {
			if (BattlePointTracker.IsCountdownTimerRunning)
            {
				BattlePointTracker.TimerStart += Time.unscaledDeltaTime;
				BattlePointTracker.TimerRemaining = BattlePointTracker.TimerEnd - BattlePointTracker.TimerStart;

				if (BattlePointTracker.TimerRemaining <= 0)
                {
					BattlePointTracker.TimerRemaining = 0;
					BattlePointTracker.IsCountdownTimerRunning = false;
					BattlePointTracker.HasStartedCastlegeon = false;
                }
            }
		}
    }
}
