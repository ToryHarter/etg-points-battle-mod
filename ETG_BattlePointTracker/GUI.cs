﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace ETG_BattlePointTracker
{
	// Humbly borrowed from: https://github.com/An3s079/DPSMod/blob/main/GUI.cs
	public static class GUI
	{
		public static bool Toggle()
		{
			bool flag = !GUI.GUIRoot.gameObject.activeSelf;
			GUI.GUIRoot.gameObject.SetActive(flag);
			return flag;
		}

		public static void SetVisible(bool visible)
		{
			GUI.GUIRoot.gameObject.SetActive(visible);
		}

		public static void Init()
		{
			GUI.GUIController = new GameObject("GUIController").transform;
			UnityEngine.Object.DontDestroyOnLoad(GUI.GUIController.gameObject);
			GUI.CreateCanvas();
			GUI.GUIRoot = GUI.m_canvas.transform;
			GUI.GUIRoot.SetParent(GUI.GUIController);
		}

		public static void CreateCanvas()
		{
			GameObject gameObject = new GameObject("Canvas");
			UnityEngine.Object.DontDestroyOnLoad(gameObject);
			GUI.m_canvas = gameObject.AddComponent<Canvas>();
			GUI.m_canvas.renderMode = RenderMode.ScreenSpaceOverlay;
			GUI.m_canvas.sortingOrder = 100000;
			gameObject.AddComponent<CanvasScaler>();
			gameObject.AddComponent<GraphicRaycaster>();
		}

		public static Text CreateText(Transform parent, Vector2 offset, string text, TextAnchor anchor = TextAnchor.MiddleCenter, int font_size = 20)
		{
			GameObject gameObject = new GameObject("Text");
			gameObject.transform.SetParent((parent != null) ? parent : GUI.GUIRoot);
			RectTransform rectTransform = gameObject.AddComponent<RectTransform>();
			rectTransform.SetTextAnchor(anchor);
			rectTransform.anchoredPosition = offset;
			Text text2 = gameObject.AddComponent<Text>();
			text2.horizontalOverflow = HorizontalWrapMode.Overflow;
			text2.verticalOverflow = VerticalWrapMode.Overflow;
			text2.alignment = anchor;
			text2.text = text;
			text2.font = ResourceManager.LoadAssetBundle("shared_auto_001").LoadAsset<Font>("04B_03__");
			text2.fontSize = font_size;
			text2.color = Color.white;
			return text2;
		}

		public static void UpdateUIOffset()
		{
			var width = Screen.width;
			var height = Screen.height;
			int num = BraveMathCollege.GreatestCommonDivisor(Screen.width, Screen.height);
			int num2 = Screen.width / num;
			int num3 = Screen.height / num;
			bool isBestAspectRatio = num2 == 16 && num3 == 9;
			float a = (float)Screen.width / 480f;
			float b = (float)Screen.height / 270f;
			bool isPixelPerfect = Mathf.Min(a, b) % 1f == 0f;


			// Listen, I know.
			// I know this is a tower of awful
			// I know there's probably a better way to do this
			// Having auto-scaling text based off ETG's resolution would be great
			// But that would require me to figure that out
			// I don't have a lot of time, so this is how I get this working now
			// Down the road I hope to right this wrong
			// But if I don't, eh, whatever
			Vector2 pointsOffset;
			Vector2 timerOffset;
			int fontSize;
			if (width == 2560 && height == 1440 && isBestAspectRatio && !isPixelPerfect)
			{
				pointsOffset = new Vector2(100f, 315f);
				timerOffset = new Vector2(100f, 255f);
				fontSize = 42;
			}
			else if (width == 1920 && height == 1440 && !isBestAspectRatio && isPixelPerfect)
			{
				pointsOffset = new Vector2(15f, 250f);
				timerOffset = new Vector2(15f, 195f);
				fontSize = 42;
			}
			else if (width == 1920 && height == 1200 && !isBestAspectRatio && isPixelPerfect)
			{
				pointsOffset = new Vector2(15f, 250f);
				timerOffset = new Vector2(15f, 200f);
				fontSize = 38;
			}
			else if (width == 1920 && height == 1080 && isBestAspectRatio && isPixelPerfect)
			{
				pointsOffset = new Vector2(15f, 250f);
				timerOffset = new Vector2(15f, 200f);
				fontSize = 38;
			}
			else if (width == 1768 && height == 992 && !isPixelPerfect)
			{
				pointsOffset = new Vector2(175f, 185f);
				timerOffset = new Vector2(175f, 135f);
				fontSize = 32;
			}
			else if (width == 1680 && height == 1050 && !isPixelPerfect)
			{
				pointsOffset = new Vector2(130f, 185f);
				timerOffset = new Vector2(130f, 140f);
				fontSize = 32;
			}
			else if (width == 1600 && height == 1200 && !isPixelPerfect)
			{
				pointsOffset = new Vector2(92f, 185f);
				timerOffset = new Vector2(92f, 140f);
				fontSize = 32;
			}
			else if (width == 1600 && height == 1024 && !isPixelPerfect)
			{
				pointsOffset = new Vector2(90f, 185f);
				timerOffset = new Vector2(90f, 140f);
				fontSize = 32;
			}
			else if (width == 1600 && height == 900 && !isPixelPerfect)
			{
				pointsOffset = new Vector2(90f, 185f);
				timerOffset = new Vector2(90f, 140f);
				fontSize = 30;
			}
			else if (width == 1440 && height == 900 && !isBestAspectRatio && isPixelPerfect)
			{
				pointsOffset = new Vector2(10f, 185f);
				timerOffset = new Vector2(10f, 140f);
				fontSize = 28;
			}
			else if (width == 1366 && height == 768 && !isPixelPerfect)
			{
				pointsOffset = new Vector2(210f, 120f);
				timerOffset = new Vector2(210f, 90f);
				fontSize = 20;
			}
			else if (width == 1360 && height == 768 && !isPixelPerfect)
			{
				pointsOffset = new Vector2(207f, 120f);
				timerOffset = new Vector2(207f, 90f);
				fontSize = 20;
			}
			else if (width == 1280 && height == 1024 && !isPixelPerfect)
			{
				pointsOffset = new Vector2(167f, 120f);
				timerOffset = new Vector2(167f, 90f);
				fontSize = 20;
			}
			else if (width == 1280 && height == 960 && !isPixelPerfect)
			{
				pointsOffset = new Vector2(167f, 120f);
				timerOffset = new Vector2(167f, 90f);
				fontSize = 22;
			}
			else if (width == 1280 && height == 800 && !isPixelPerfect)
			{
				pointsOffset = new Vector2(167f, 120f);
				timerOffset = new Vector2(167f, 90f);
				fontSize = 20;
			}
			else if (width == 1280 && height == 768 && !isPixelPerfect)
			{
				pointsOffset = new Vector2(167f, 120f);
				timerOffset = new Vector2(167f, 90f);
				fontSize = 20;
			}
			else if (width == 1280 && height == 720 && !isPixelPerfect)
			{
				pointsOffset = new Vector2(167f, 120f);
				timerOffset = new Vector2(167f, 90f);
				fontSize = 20;
			}
			else if (width == 1176 && height == 664 && !isPixelPerfect)
			{
				pointsOffset = new Vector2(115f, 120f);
				timerOffset = new Vector2(115f, 90f);
				fontSize = 20;
			}
			else if (width == 1152 && height == 864 && !isPixelPerfect)
			{
				pointsOffset = new Vector2(103f, 120f);
				timerOffset = new Vector2(103f, 90f);
				fontSize = 20;
			}
			else if (width == 1024 && height == 768 && !isPixelPerfect)
			{
				pointsOffset = new Vector2(40f, 120f);
				timerOffset = new Vector2(40f, 90f);
				fontSize = 20;
			}
			else if (width == 800 && height == 600 && !isPixelPerfect)
			{
				pointsOffset = new Vector2(163f, 60f);
				timerOffset = new Vector2(163f, 45f);
				fontSize = 10;
			}
			else if (width == 720 && height == 576 && !isPixelPerfect)
			{
				pointsOffset = new Vector2(124f, 60f);
				timerOffset = new Vector2(124f, 45f);
				fontSize = 10;
			}
			else if (width == 720 && height == 480 && !isPixelPerfect)
			{
				pointsOffset = new Vector2(124f, 60f);
				timerOffset = new Vector2(124f, 45f);
				fontSize = 10;
			}
			else if (width == 640 && height == 480 && !isPixelPerfect)
			{
				pointsOffset = new Vector2(83f, 60f);
				timerOffset = new Vector2(83f, 45f);
				fontSize = 10;
			}
			else
            {
				pointsOffset = new Vector2(100f, 315f);
				timerOffset = new Vector2(100f, 255f);
				fontSize = 42;
			}

			var pointsRectTransform = BattlePointTracker.PointsText.gameObject.GetComponent<RectTransform>();
			pointsRectTransform.anchoredPosition = pointsOffset;
			var timerRectTransform = BattlePointTracker.CountdownTimer.gameObject.GetComponent<RectTransform>();
			timerRectTransform.anchoredPosition = timerOffset;
			BattlePointTracker.PointsText.fontSize = fontSize;
			BattlePointTracker.CountdownTimer.fontSize = fontSize;
		}

		public static void SetTextAnchor(this RectTransform r, TextAnchor anchor)
		{
			r.anchorMin = GUI.AnchorMap[anchor];
			r.anchorMax = GUI.AnchorMap[anchor];
			r.pivot = GUI.AnchorMap[anchor];
		}

		public static Font font;

		public static Transform GUIRoot;

		public static Transform GUIController;

		private static Canvas m_canvas;

		public static readonly Dictionary<TextAnchor, Vector2> AnchorMap = new Dictionary<TextAnchor, Vector2>
		{
			{
				TextAnchor.LowerLeft,
				new Vector2(0f, 0f)
			},
			{
				TextAnchor.LowerCenter,
				new Vector2(0.5f, 0f)
			},
			{
				TextAnchor.LowerRight,
				new Vector2(1f, 0f)
			},
			{
				TextAnchor.MiddleLeft,
				new Vector2(0f, 0.5f)
			},
			{
				TextAnchor.MiddleCenter,
				new Vector2(0.5f, 0.5f)
			},
			{
				TextAnchor.MiddleRight,
				new Vector2(1f, 0.5f)
			},
			{
				TextAnchor.UpperLeft,
				new Vector2(0f, 1f)
			},
			{
				TextAnchor.UpperCenter,
				new Vector2(0.5f, 1f)
			},
			{
				TextAnchor.UpperRight,
				new Vector2(1f, 1f)
			}
		};

		private static Color defaultTextColor = new Color(1f, 1f, 1f, 0.5f);
	}
}