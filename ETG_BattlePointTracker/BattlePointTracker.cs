﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MonoMod.RuntimeDetour;
using MonoMod.Utils;
using System.Reflection;
using Dungeonator;
using UnityEngine;
using HutongGames.PlayMaker.Actions;
using System.Collections;
using static PunchoutAIActor;

namespace ETG_BattlePointTracker
{
    public class BattlePointTracker : ETGModule
    {
		public const float DEFAULT_TIMER_DURATION = 90;
		public const int WINCHESTER_TARGETS_TO_BREAK = 2;

		public static UnityEngine.UI.Text PointsText;
		public static UnityEngine.UI.Text CountdownTimer;
		public static int Points;
		public static bool IsCountdownTimerRunning;
		public static float TimerStart;
		public static float TimerEnd;
		public static float TimerRemaining;
		public static float TimerDurationInMinutes;
		public static bool HasStartedCastlegeon;


		private int FontSize = 42;

		public delegate T7 CustomFunc<T1, T2, T3, T4, T5, T6, T7>(T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6);

		public override void Init() {}
		public override void Exit() {}

		public override void Start()
        {
            Points = 0;
			IsCountdownTimerRunning = false;
			TimerDurationInMinutes = DEFAULT_TIMER_DURATION;
			TimerStart = 0;
			TimerEnd = TimerDurationInMinutes * 60;
			TimerRemaining = TimerEnd - TimerStart;
			HasStartedCastlegeon = false;

			this.SetupHooks();
			this.SetupUI();
			ConsoleCommands.AddPBTCommands();
		}

        private void SetupHooks()
        {
			// Hook for Updating UI Elements when Resolution Changes
			new Hook(
				typeof(BraveOptionsMenuItem).GetMethod("HandleResolutionChanged", BindingFlags.NonPublic | BindingFlags.Instance),
				typeof(BattlePointTracker).GetMethod("Hook_BraveOptionsMenuItem_HandleResolutionChanged", BindingFlags.NonPublic | BindingFlags.Instance),
				this);

			// Hook for getting bool if timer has started
			new Hook(
				typeof(PlayerController).GetMethod("LateUpdate", BindingFlags.NonPublic | BindingFlags.Instance),
				typeof(BattlePointTracker).GetMethod("Hook_PlayerController_LateUpdate", BindingFlags.NonPublic | BindingFlags.Instance),
				this);

			// Hook for Defeating Bosses + Master Rounds
			new Hook(
				typeof(RoomHandler).GetMethod("HandleBossClearReward", BindingFlags.NonPublic | BindingFlags.Instance),
				typeof(BattlePointTracker).GetMethod("Hook_RoomHandler_HandleBossClearReward", BindingFlags.NonPublic | BindingFlags.Instance),
				this);

			// Hook for Defeating the Resourceful Rat's Second Phase
			new Hook(
				typeof(MetalGearRatDeathController).GetMethod("OnBossDeath", BindingFlags.NonPublic | BindingFlags.Instance),
				typeof(BattlePointTracker).GetMethod("Hook_MetaGearRatDeathController_OnBossDeath", BindingFlags.NonPublic | BindingFlags.Instance),
				this);

			// Hook for Defeating the Bullet Hell Lich
			new Hook(
				typeof(InfinilichDeathController).GetMethod("OnBossDeath", BindingFlags.NonPublic | BindingFlags.Instance),
				typeof(BattlePointTracker).GetMethod("Hook_InfinilichDeathController_OnBossDeath", BindingFlags.NonPublic | BindingFlags.Instance),
				this);

			// Hook for Beating Punchout
			new Hook(
				typeof(PunchoutAIActor.DeathState).GetMethod("Start", BindingFlags.Public | BindingFlags.Instance),
				typeof(BattlePointTracker).GetMethod("Hook_DeathState_Start", BindingFlags.Public | BindingFlags.Instance),
				this);

			// Hook for reaching Credits
			new Hook(
				typeof(TimeTubeCreditsController).GetMethod("HandleTimeTubeCredits", BindingFlags.Public | BindingFlags.Instance),
				typeof(BattlePointTracker).GetMethod("Hook_TimeTubeCreditsController_HandleTimeTubeCredits", BindingFlags.Public | BindingFlags.Instance),
				this);

			// Hook for Defeating Mini-Bosses
			new Hook(
				typeof(HealthHaver).GetMethod("FinalizeDeath", BindingFlags.NonPublic | BindingFlags.Instance),
				typeof(BattlePointTracker).GetMethod("Hook_HealthHaver_FinalizeDeath", BindingFlags.NonPublic | BindingFlags.Instance),
				this);

			// Hook for Completing Gunslinger King Mini-Game
			new Hook(
				typeof(CheckGunslingChallengeComplete).GetMethod("InformManservantSuccess", BindingFlags.NonPublic | BindingFlags.Instance),
				typeof(BattlePointTracker).GetMethod("Hook_CheckGunslingChallengeComplete_InformManservantSuccess", BindingFlags.NonPublic | BindingFlags.Instance),
				this);

			// Hook for Completing Lost Adventurer Mini-Game
			new Hook(
				typeof(CheckEntireFloorVisited).GetMethod("OnEnter", BindingFlags.Public | BindingFlags.Instance),
				typeof(BattlePointTracker).GetMethod("Hook_CheckEntireFloorVisited_OnEnter", BindingFlags.Public | BindingFlags.Instance),
				this);

			// Hook for breaking at least two targets in Winchester's Mini-Game
			new Hook(
				typeof(ArtfulDodgerRoomController).GetMethod("DoHandleReward", BindingFlags.Public | BindingFlags.Instance),
				typeof(BattlePointTracker).GetMethod("Hook_ArtfulDodgerRoomController_DoHandleReward", BindingFlags.Public | BindingFlags.Instance),
				this);

			// Hook for Entering Secret Floor + If starting a new run
			new Hook(
				typeof(LevelNameUIManager).GetMethod("ShowLevelName", BindingFlags.Public | BindingFlags.Instance),
				typeof(BattlePointTracker).GetMethod("Hook_LevelNameUIManager_ShowLevelName", BindingFlags.Public | BindingFlags.Instance),
				this);
		}

		private void SetupUI()
        {
			// Humbly borrowed from: https://github.com/An3s079/DPSMod/blob/main/DPSmod.cs
			GUI.Init();

			PointsText = GUI.CreateText(null, new Vector2(0f, 0f), "", TextAnchor.MiddleLeft, font_size: FontSize);
			CountdownTimer = GUI.CreateText(null, new Vector2(100f, 100f), "", TextAnchor.MiddleLeft, font_size: FontSize);
			GUI.UpdateUIOffset();
			ETGModMainBehaviour.Instance.gameObject.AddComponent<PointUpdater>();
			ETGModMainBehaviour.Instance.gameObject.AddComponent<CountdownTimer>();
		}

		public void SetPoints(int value)
        {
			Points = value;
        }

		public void AddPoints(int pointsToAdd)
        {
			if (IsCountdownTimerRunning)
            {
				this.SetPoints(Points + pointsToAdd);
            }
        }

		// Hook for Updating UI Elements when Resolution Changes
		private void Hook_BraveOptionsMenuItem_HandleResolutionChanged(Action<BraveOptionsMenuItem, dfControl, Vector3, Vector3> orig, BraveOptionsMenuItem self, dfControl arg1, Vector3 arg2, Vector3 arg3)
        {
			Console.WriteLine("ETG_BattlePointTracker - Starting Hook_BraveOptionsMenuItem_HandleResolutionChanged");

			orig(self, arg1, arg2, arg3);

			GUI.UpdateUIOffset();
		}

		private void Hook_PlayerController_LateUpdate(Action<PlayerController> orig, PlayerController self)
        {
			if (!IsCountdownTimerRunning)
            {
				FieldInfo newFloorNoInputInfo = typeof(PlayerController).GetField("m_newFloorNoInput", BindingFlags.NonPublic | BindingFlags.Instance);
				bool newFloorNoInput = (bool)newFloorNoInputInfo.GetValue(self);

				IsCountdownTimerRunning = !newFloorNoInput && GameManager.Instance.CurrentLevelOverrideState != GameManager.LevelOverrideState.FOYER && HasStartedCastlegeon;

				if (IsCountdownTimerRunning)
                {
					TimerStart = 0;
					TimerEnd = TimerDurationInMinutes * 60;
					TimerRemaining = TimerEnd - TimerStart;
				}
            }

			orig(self);
        }

		// Points for Defeating Bosses + Master Rounds
		protected virtual void Hook_RoomHandler_HandleBossClearReward(Action<RoomHandler> orig, RoomHandler self)
		{
			Console.WriteLine("ETG_BattlePointTracker - Starting Hook_RoomHandler_HandleBossClearReward");

			int floorBossDefeatedPointReward = 0;
			int floorMasterRoundPointReward = 0;
			switch (GameManager.Instance.Dungeon.tileIndices.tilesetId)
            {
				case GlobalDungeonData.ValidTilesets.CASTLEGEON: // Keep of the Lead Lord
					floorBossDefeatedPointReward = PointSheet.BOSS_KEEP_OF_THE_LEAD_LORD;
					floorMasterRoundPointReward = PointSheet.MASTER_ROUND_KEEP_OF_THE_LEAD_LORD;
					break;
				case GlobalDungeonData.ValidTilesets.GUNGEON: // Gungeon Proper
					floorBossDefeatedPointReward = PointSheet.BOSS_GUNGEON_PROPER;
					floorMasterRoundPointReward = PointSheet.MASTER_ROUND_GUNGEON_PROPER;
					break;
				case GlobalDungeonData.ValidTilesets.MINEGEON: // Black Powder Mine
					floorBossDefeatedPointReward = PointSheet.BOSS_FLOOR_BLACK_POWDER_MINE;
					floorMasterRoundPointReward = PointSheet.MASTER_ROUND_BLACK_POWDER_MINE;
					break;
				case GlobalDungeonData.ValidTilesets.CATACOMBGEON: // Hollow
					floorBossDefeatedPointReward = PointSheet.BOSS_FLOOR_HOLLOW;
					floorMasterRoundPointReward = PointSheet.MASTER_ROUND_HOLLOW;
					break;
				case GlobalDungeonData.ValidTilesets.FORGEGEON: // Forge
					floorBossDefeatedPointReward = PointSheet.BOSS_FLOOR_FORGE;
					floorMasterRoundPointReward = PointSheet.MASTER_ROUND_FORGE;
					break;
				case GlobalDungeonData.ValidTilesets.SEWERGEON: // Oubliette
					floorBossDefeatedPointReward = PointSheet.BOSS_OUBLIETTE;
					break;
				case GlobalDungeonData.ValidTilesets.CATHEDRALGEON: // Abbey of the True Gun
					floorBossDefeatedPointReward = PointSheet.BOSS_ABBEY_OF_THE_TRUE_GUN;
					break;
				case GlobalDungeonData.ValidTilesets.OFFICEGEON: // R&G Dept.
					floorBossDefeatedPointReward = PointSheet.BOSS_R_AND_G_DEPT;
					break;
			}

			Console.WriteLine("ETG_BattlePointTracker - Adding point for beating boss");
			this.AddPoints(floorBossDefeatedPointReward);
			Console.WriteLine($"ETG_BattlePointTracker - {Points} Points");

			bool earnedMasterRound = !self.PlayerHasTakenDamageInThisRoom && GameManager.Instance.Dungeon.BossMasteryTokenItemId >= 0 && !GameManager.Instance.Dungeon.HasGivenMasteryToken;
			if (earnedMasterRound)
            {
				Console.WriteLine("ETG_BattlePointTracker - Adding point for getting Master Round");
				this.AddPoints(floorMasterRoundPointReward);
				Console.WriteLine($"ETG_BattlePointTracker - {Points} Points");
			}

			orig(self);
		}

		// Points for Defeating the Resourceful Rat's Second Phase
		private void Hook_MetaGearRatDeathController_OnBossDeath(Action<MetalGearRatDeathController, Vector2> orig, MetalGearRatDeathController self, Vector2 dir)
        {
			Console.WriteLine("ETG_BattlePointTracker - Starting Hook_MetaGearRatDeathController_OnBossDeath");

			Console.WriteLine("ETG_BattlePointTracker - Adding points for beating Resourceful Rat");
			this.AddPoints(PointSheet.BOSS_RATS_LAIR);
			Console.WriteLine($"ETG_BattlePointTracker - {Points} Points");

			orig(self, dir);
        }

		// Points for Defeating the Bullet Hell Lich
		private void Hook_InfinilichDeathController_OnBossDeath(Action<InfinilichDeathController, Vector2> orig, InfinilichDeathController self, Vector2 dir)
        {
			Console.WriteLine("ETG_BattlePointTracker - Starting Hook_InfinilichDeathController_OnBossDeath");

			Console.WriteLine("ETG_BattlePointTracker - Adding points for beating Lich");
			this.AddPoints(PointSheet.BOSS_BULLET_HELL);
			Console.WriteLine($"ETG_BattlePointTracker - {Points} Points");

			orig(self, dir);
        }

		// Points for Beating Punchout
		public void Hook_DeathState_Start(Action<PunchoutAIActor.DeathState> orig, PunchoutAIActor.DeathState self)
        {
			Console.WriteLine("ETG_BattlePointTracker - Starting Hook_DeathState_Start");

			orig(self);

			Console.WriteLine("ETG_BattlePointTracker - Adding points for Beating Punchout");
			this.AddPoints(PointSheet.BEATING_PUNCHOUT);
			Console.WriteLine($"ETG_BattlePointTracker - {Points} Points");
		}

		// Points for reaching Credits
		public IEnumerator Hook_TimeTubeCreditsController_HandleTimeTubeCredits(CustomFunc<TimeTubeCreditsController, Vector2, bool, tk2dSpriteAnimator, int, bool, IEnumerator> orig, TimeTubeCreditsController self, Vector2 decayCenter, bool skipCredits, tk2dSpriteAnimator optionalAnimatorToDisable, int shotPlayerID, bool quickEndShatter = false)
        {
			Console.WriteLine("ETG_BattlePointTracker - Starting Hook_TimeTubeCreditsController_HandleTimeTubeCredits");

			Console.WriteLine("ETG_BattlePointTracker - Adding points for reaching credits");
			this.AddPoints(PointSheet.CREDITS);
			Console.WriteLine($"ETG_BattlePointTracker - {Points} Points");

			IEnumerator origEnum = orig(self, decayCenter, skipCredits, optionalAnimatorToDisable, shotPlayerID, quickEndShatter);

			while (origEnum.MoveNext())
			{
				yield return origEnum.Current;
			}
		}

		// Points for Defeating Mini-Bosses
		private void Hook_HealthHaver_FinalizeDeath(Action<HealthHaver> orig, HealthHaver self)
		{
			Console.WriteLine("ETG_BattlePointTracker - Starting Hook_HealthHaver_FinalizeDeath");

			orig(self);

			if (self.IsSubboss)
            {
				Console.WriteLine("ETG_BattlePointTracker - Adding points for defeating mini-boss");
				this.AddPoints(PointSheet.MINI_BOSSES);
				Console.WriteLine($"ETG_BattlePointTracker - {Points} Points");
			}
		}

		// Points for Completing Gunslinger King Mini-Game
		private void Hook_CheckGunslingChallengeComplete_InformManservantSuccess(Action<CheckGunslingChallengeComplete> orig, CheckGunslingChallengeComplete self)
        {
			Console.WriteLine("ETG_BattlePointTracker - Starting Hook_CheckGunslingChallengeComplete_InformManservantSuccess");

			orig(self);

			Console.WriteLine("ETG_BattlePointTracker - Adding points for completing Gunslinger King Challenge");
			this.AddPoints(PointSheet.MINIGAME_GUNSLINGER_KING);
			Console.WriteLine($"ETG_BattlePointTracker - {Points} Points");
		}

		// Points for Completing Lost Adventurer Mini-Game
		public void Hook_CheckEntireFloorVisited_OnEnter(Action<CheckEntireFloorVisited> orig, CheckEntireFloorVisited self)
		{
			Console.WriteLine("ETG_BattlePointTracker - Starting Hook_CheckEntireFloorVisited_OnEnter");

			GameObject lostAdventurerGameObject = self.Owner.GetComponent<TalkDoerLite>().gameObject;
			bool isLostAdventurer = lostAdventurerGameObject.name.StartsWith("NPC_LostAdventurer");
			bool challengeComplete = lostAdventurerGameObject.GetComponent<LostAdventurerFlag>() != null;

			if (isLostAdventurer && !challengeComplete)
			{
				var testCompletionMethod = typeof(CheckEntireFloorVisited).GetMethod("TestCompletion", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance);
				bool mapCompleted = (bool)testCompletionMethod.Invoke(self, new object[0]);

				if (mapCompleted)
				{
					lostAdventurerGameObject.AddComponent<LostAdventurerFlag>();
					Console.WriteLine("ETG_BattlePointTracker - Adding points for completing Lost Adventurer Challenge");
					this.AddPoints(PointSheet.MINIGAME_THE_LOST_ADVENTURER);
					Console.WriteLine($"ETG_BattlePointTracker - {Points} Points");
				}
			}

			orig(self);
		}

		// Points for breaking at least two targets in Winchester's Mini-Game
		public void Hook_ArtfulDodgerRoomController_DoHandleReward(Action<ArtfulDodgerRoomController> orig, ArtfulDodgerRoomController self)
        {
			Console.WriteLine("ETG_BattlePointTracker - Starting Hook_ArtfulDodgerRoomController_DoHandleReward");

			FieldInfo m_targets = typeof(ArtfulDodgerRoomController).GetField("m_targets", BindingFlags.NonPublic | BindingFlags.Instance);
			List<ArtfulDodgerTargetController> targets = (List<ArtfulDodgerTargetController>)m_targets.GetValue(self);
			int brokenTargets = targets.Count(x => x.IsBroken);

			if (brokenTargets >= WINCHESTER_TARGETS_TO_BREAK)
            {
				Console.WriteLine("ETG_BattlePointTracker - Adding points for breaking at least two targets in Winchester's Mini-Game");
				this.AddPoints(PointSheet.MINIGAME_WINCHESTER);
				Console.WriteLine($"ETG_BattlePointTracker - {Points} Points");
			}

			orig(self);
        }

		// Points for Entering Secret Floor + If starting new run
		public void Hook_LevelNameUIManager_ShowLevelName(Action<LevelNameUIManager, Dungeon> orig, LevelNameUIManager self, Dungeon d)
        {
			Console.WriteLine("ETG_BattlePointTracker - Starting Hook_LevelNameUIManager_ShowLevelName");

			int pointsToAdd = 0;
			switch (GameManager.Instance.Dungeon.tileIndices.tilesetId)
			{
				case GlobalDungeonData.ValidTilesets.CASTLEGEON:
					HasStartedCastlegeon = true;
					break;
				case GlobalDungeonData.ValidTilesets.SEWERGEON:
					pointsToAdd = PointSheet.ENTER_OUBLIETTE;
					break;
				case GlobalDungeonData.ValidTilesets.CATHEDRALGEON:
					pointsToAdd = PointSheet.ENTER_ABBEY_IF_THE_TRUE_GUN;
					break;
				case GlobalDungeonData.ValidTilesets.RATGEON:
					pointsToAdd = PointSheet.ENTER_RATS_LAIR;
					break;
				case GlobalDungeonData.ValidTilesets.OFFICEGEON:
					pointsToAdd = PointSheet.ENTER_R_AND_G_DEPT;
					break;
			}

			if (pointsToAdd != 0)
            {
				Console.WriteLine("ETG_BattlePointTracker - Adding point for Entering Secret Floor");
				this.AddPoints(pointsToAdd);
				Console.WriteLine($"ETG_BattlePointTracker - {Points} Points");
            }

			orig(self, d);
		}
	}
}
