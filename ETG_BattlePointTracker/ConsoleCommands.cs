﻿using Dungeonator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ETG_BattlePointTracker
{
    class ConsoleCommands
    {
		private const bool ENABLE_DEBUG_COMMANDS = false;

		public static void AddPBTCommands()
        {
			ETGModConsole.Commands.AddGroup("pt");
			ETGModConsole.Commands.GetGroup("pt").AddGroup("help", Help);
			ETGModConsole.Commands.GetGroup("pt").AddGroup("new", NewPointsBattle);
			ETGModConsole.Commands.GetGroup("pt").AddGroup("resettimer", ResetTimer);
			ETGModConsole.Commands.GetGroup("pt").AddGroup("timer", SetTimerDuration);
			ETGModConsole.Commands.GetGroup("pt").AddGroup("clearpoints", ClearPoints);

			if (ENABLE_DEBUG_COMMANDS)
            {
				ETGModConsole.Commands.AddGroup("viewall", ViewAll);
				ETGModConsole.Commands.AddGroup("os", SetUIOffset);
				ETGModConsole.Commands.AddGroup("fs", SetUIFontSize);
            }
        }

		// Humbly borrowed from: https://modworkshop.net/mod/23271
		private static void Help(string[] args)
        {
			try
            {
				ETGModConsole.Log("");
				ETGModConsole.Log("<size=100><color=#ff0000ff>Points Battle Tracker</color></size>");
				ETGModConsole.Log("<size=100><color=#ff0000ff>By The_Rot_Bot</color></size>");
				ETGModConsole.Log("<size=100><color=#ff0000ff>--------------------------------</color></size>");
				ETGModConsole.Log("Points Battle Tracker Commands:");
				ETGModConsole.Log("");
				ETGModConsole.Log("<color=#FFFF33>pt help</color> - lists Points Battle Tracker commands");
				ETGModConsole.Log("<color=#FFFF33>pt new</color> - clears points and resets timer");
				ETGModConsole.Log("<color=#FFFF33>pt resettimer</color> - resets the timer to the timer duration");
				ETGModConsole.Log("<color=#FFFF33>pt timer [minutes]</color> - sets the timer duration to the amount provided in minutes");
				ETGModConsole.Log("<color=#FFFF33>pt clearpoints</color> - clears points back to zero");
			}
			catch (Exception e)
            {
				ETGModConsole.Log($"Something went wrong showing help: {e.Message}");
				ETGModConsole.Log(e.InnerException.Message);
			}
        }

		private static void NewPointsBattle(string[] args)
        {
			try
			{
				ClearPoints(new string[0]);
				ResetTimer(new string[0]);
			}
			catch (Exception e)
			{
				ETGModConsole.Log($"Something went wrong starting a new Points Battle: {e.Message}");
				ETGModConsole.Log(e.InnerException.Message);
			}
		}

		private static void ResetTimer(string[] args)
        {
			try
            {
				BattlePointTracker.IsCountdownTimerRunning = false;
				BattlePointTracker.HasStartedCastlegeon = false;
				BattlePointTracker.TimerStart = 0;
				BattlePointTracker.TimerEnd = BattlePointTracker.TimerDurationInMinutes * 60;
				BattlePointTracker.TimerRemaining = BattlePointTracker.TimerEnd - BattlePointTracker.TimerStart;
            }
			catch (Exception e)
            {
				ETGModConsole.Log($"Something went wrong reseting the timer: {e.Message}");
				ETGModConsole.Log(e.InnerException.Message);
			}
        }

		private static void SetTimerDuration(string[] args)
        {
			try
            {
				if (args.Length >= 1)
                {
					var timerDurationInMinutes = float.Parse((args[0]));

					BattlePointTracker.TimerDurationInMinutes = timerDurationInMinutes;

					ETGModConsole.Log($"Timer Duration Updated: {BattlePointTracker.TimerDurationInMinutes} minutes");
                }
            }
			catch (Exception e)
            {
				ETGModConsole.Log($"Something went wrong setting the timer duration: {e.Message}");
				ETGModConsole.Log(e.InnerException.Message);
			}
        }

		private static void SetUIFontSize(string[] args)
        {
			try
            {
				if (args.Length >= 1)
                {
					var fontSize = int.Parse(args[0]);

					BattlePointTracker.PointsText.fontSize = fontSize;

					ETGModConsole.Log($"FontSize Updated: {fontSize}");
				}
            }
			catch (Exception e)
            {
				ETGModConsole.Log($"Something went wrong setting the UI font size: {e.Message}");
				ETGModConsole.Log(e.InnerException.Message);
			}
        }

		private static void SetUIOffset(string[] args)
        {
			try
            {
				if (args.Length >= 2)
                {
					var xPos = float.Parse(args[0]);
					var yPos = float.Parse(args[1]);

					var rectTransform = BattlePointTracker.CountdownTimer.gameObject.GetComponent<RectTransform>();
					rectTransform.anchoredPosition = new Vector2(xPos, yPos);

					ETGModConsole.Log($"Offset Updated: {xPos} x {yPos}");
				}
            }
			catch (Exception e)
            {
				ETGModConsole.Log($"Something went wrong setting the UI offset: {e.Message}");
				ETGModConsole.Log(e.InnerException.Message);
			}
        }

		private static void ClearPoints(string[] args)
        {
			try
            {
				BattlePointTracker.Points = 0;
            }
			catch (Exception e)
            {
				ETGModConsole.Log($"Something went wrong clearing points: {e.Message}");
				ETGModConsole.Log(e.InnerException.Message);
			}
        }

		private static void ViewAll(string[] args)
		{
			bool verbose = false;
			bool lostAdventurerFound = false;
			int hasHierarchyParent = 0;
			int noHierarchyParent = 0;

			try
			{
				RoomHandler currentRoom = GameManager.Instance.PrimaryPlayer.CurrentRoom;
				ETGModConsole.Log($"Current Room Type: {currentRoom.area.PrototypeRoomCategory.ToString()}");
				if (currentRoom.area.PrototypeRoomCategory == PrototypeDungeonRoom.RoomCategory.SPECIAL)
				{
					ETGModConsole.Log($"Special Room Type: {currentRoom.area.PrototypeRoomSpecialSubcategory.ToString()}");
				}


				if (verbose) ETGModConsole.Log("Searching Rooms...");
				List<RoomHandler> allRooms = GameManager.Instance.Dungeon.data.rooms;
				allRooms = allRooms.Where(x => x.area.PrototypeRoomCategory == PrototypeDungeonRoom.RoomCategory.SPECIAL && x.area.PrototypeRoomSpecialSubcategory == PrototypeDungeonRoom.RoomSpecialSubCategory.UNSPECIFIED_SPECIAL).ToList();

				if (GameManager.Instance?.Dungeon?.data?.rooms == null)
				{
					if (verbose) ETGModConsole.Log("Null Room data!");
					return;
				}

				foreach (RoomHandler room in allRooms)
				{
					if (verbose) ETGModConsole.Log("Looping Room...");
					if (room.hierarchyParent == null)
					{
						if (verbose) ETGModConsole.Log("Null HierarchyParent");
						ETGModConsole.Log("");
						ETGModConsole.Log($"Category: {room.area.PrototypeRoomCategory.ToString()}");
						ETGModConsole.Log($"Normal Sub: {room.area.PrototypeRoomNormalSubcategory.ToString()}");
						ETGModConsole.Log($"Special Sub: {room.area.PrototypeRoomSpecialSubcategory.ToString()}");
						ETGModConsole.Log("");
						noHierarchyParent++;
						continue;
					}
					hasHierarchyParent++;
					foreach (Transform item in room.hierarchyParent)
					{
						if (verbose) ETGModConsole.Log("Got Room Transform...");
						//ETGModConsole.Log(item.ToString());
						//if (item)
						if (item && item.ToString().ToLower().Contains("adventurer"))
						{
							foreach (var item2 in item.GetComponentsInChildren(typeof(Component)))
							{
								ETGModConsole.Log(item2.ToString());
								lostAdventurerFound = true;
							}
						}
					}
				}
				if (verbose) ETGModConsole.Log("Search Complete!");
				ETGModConsole.Log($"HierarchyParent Rooms: {hasHierarchyParent.ToString()}");
				ETGModConsole.Log($"No HierarchyParent Rooms: {noHierarchyParent.ToString()}");
				ETGModConsole.Log($"Lost Adventurer Found: {lostAdventurerFound.ToString()}");
			}
			catch (Exception e)
			{
				ETGModConsole.Log($"Error searching rooms: {e.Message}");
				ETGModConsole.Log(e.InnerException.Message);
			}
		}
	}
}
