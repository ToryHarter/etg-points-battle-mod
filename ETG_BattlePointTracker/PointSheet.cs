﻿namespace ETG_BattlePointTracker
{
    public static class PointSheet
    {
        public const int BOSS_KEEP_OF_THE_LEAD_LORD = 1;
        public const int MASTER_ROUND_KEEP_OF_THE_LEAD_LORD = 1;
        public const int BOSS_OUBLIETTE = 1;
        public const int BOSS_GUNGEON_PROPER = 1;
        public const int MASTER_ROUND_GUNGEON_PROPER = 1;
        public const int BOSS_ABBEY_OF_THE_TRUE_GUN = 2;
        public const int BOSS_FLOOR_BLACK_POWDER_MINE = 1;
        public const int MASTER_ROUND_BLACK_POWDER_MINE = 1;
        public const int BOSS_RATS_LAIR = 2;
        public const int BEATING_PUNCHOUT = 1;
        public const int BOSS_FLOOR_HOLLOW = 1;
        public const int MASTER_ROUND_HOLLOW = 1;
        public const int BOSS_R_AND_G_DEPT = 2;
        public const int BOSS_FLOOR_FORGE = 1;
        public const int MASTER_ROUND_FORGE = 1;
        public const int BOSS_BULLET_HELL = 4;
        public const int CREDITS = 2;
        public const int MINI_BOSSES = 1;
        public const int MINIGAME_WINCHESTER = 1;
        public const int MINIGAME_GUNSLINGER_KING = 1;
        public const int MINIGAME_THE_LOST_ADVENTURER = 1;
        public const int ENTER_OUBLIETTE = 1;
        public const int ENTER_ABBEY_IF_THE_TRUE_GUN = 1;
        public const int ENTER_RATS_LAIR = 1;
        public const int ENTER_R_AND_G_DEPT = 1;
    }
}
